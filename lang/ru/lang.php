<?php

return [
    'plugin' => [
        'name' => 'Vite Helper',
        'description' => 'Плагин упрощает настройку сборки стилей и скриптов темы через Vite.',
    ],
];

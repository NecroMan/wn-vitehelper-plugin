<?php

return [
    'plugin' => [
        'name' => 'Vite Helper',
        'description' => 'The plugin simplifies the configuration of the building of styles and scripts of the theme through Vite.',
    ],
];

# Installation
You can install plugin with Composer:

```shell
composer require "as-milano/wn-vitehelper-plugin"
```

# How to use

## Component

Add component `viteAssets` to your layout or another place.
```
{% component 'viteAssets' %}
```

You can split asset to two parts for better placement:
```
{% component 'viteAssets' render='css' %}
{% component 'viteAssets' render='js' %}
```

## Configuration

There are few options for your `.env` file to configure development and building process:

```dotenv
VITE_ENTRY_FILE="js/index.js" # Entry file of your Vite project
VITE_THEME_PATH="/themes/milano" # Path to your active theme
VITE_PUPLIC_PATH="/src/public" # Path to "public" files, needed for dev mode
VITE_MANIFEST_DIR="assets/.vite" # Folder for manifest placement
VITE_ASSETS_DIR="assets" # Folder for assets 
VITE_SERVE="auto" # Type of active serve mode detection. Can be: true | false | auto
VITE_SERVE_ADDRESS="http://host.docker.internal:5173" # Serve "external" address
VITE_SERVE_TIMEOUT=1 # Auto detection mode timeout
```

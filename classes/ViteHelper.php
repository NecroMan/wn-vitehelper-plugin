<?php namespace ASMilano\ViteHelper\Classes;

use Cms\Classes\Theme;
use Winter\Storm\Filesystem\PathResolver;
use Winter\Storm\Network\Http;

class ViteHelper extends \Winter\Storm\Support\Singleton
{
    private bool $viteServed;
    private string $serveAddress;
    private string $entryFile;
    private string $manifestDir;
    private string $assetsDir;
    private string $themePath;
    private string $publicPath;
    private array $js = [];
    private array $css = [];

    protected function init(): void
    {
        $this->serveAddress = env('VITE_SERVE_ADDRESS', 'http://127.0.0.1:5173');
        $this->themePath = env('VITE_THEME_PATH', '/themes/milano');
        $this->publicPath = env('VITE_PUBLIC_PATH', '/src/public');
        $this->entryFile = env('VITE_ENTRY_FILE', 'js/index.js');
        $this->manifestDir = env('VITE_MANIFEST_DIR', 'assets');
        $this->assetsDir = env('VITE_ASSETS_DIR', 'assets');

        $preferServe = env('VITE_SERVE');
        $this->viteServed =
            $preferServe === true ||
            ($preferServe === 'auto' &&
                Http::get($this->getDevUrl(), static function (Http $http) {
                    $serveTimeout = env('VITE_SERVE_TIMEOUT', 3);
                    $http->timeout($serveTimeout);
                })->ok);

        $this->parseManifest();
    }

    public function getIsViteServed(): bool
    {
        return $this->viteServed;
    }

    public function getDevUrl(): string
    {
        return $this->serveAddress;
    }

    public function getPublicUrl(): string
    {
        return $this->getDevUrl() . $this->themePath . $this->publicPath;
    }

    public function getEntryPath(): string
    {
        return $this->entryFile;
    }

    public function getJs(): array
    {
        return $this->js;
    }

    public function getCss(): array
    {
        return $this->css;
    }

    private function parseManifest(): void
    {
        $path = PathResolver::resolve(
            Theme::getActiveTheme()->getPath() .
                DIRECTORY_SEPARATOR .
                $this->manifestDir .
                DIRECTORY_SEPARATOR .
                'manifest.json',
        );

        if (!file_exists($path)) {
            return;
        }

        $manifest = json_decode(file_get_contents($path), true, 512, JSON_THROW_ON_ERROR);

        if (!array_key_exists($this->entryFile, $manifest)) {
            return;
        }

        $this->js = array_map(
            function ($item) {
                return $this->assetsDir . '/' . $item;
            },
            [$manifest[$this->entryFile]['file']],
        );

        $this->css = array_map(function ($item) {
            return $this->assetsDir . '/' . $item;
        }, $manifest[$this->entryFile]['css']);
    }
}

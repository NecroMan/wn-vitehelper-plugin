<?php namespace ASMilano\ViteHelper;

use ASMilano\ViteHelper\Components\ViteAssets;
use System\Classes\PluginBase;

/**
 * ViteHelper Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     */
    public function pluginDetails(): array
    {
        return [
            'name' => 'asmilano.vitehelper::lang.plugin.name',
            'description' => 'asmilano.vitehelper::lang.plugin.description',
            'author' => 'АС "Милано"',
            'icon' => 'icon-leaf',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register(): void
    {
    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot(): void
    {
    }

    /**
     * Registers any frontend components implemented in this plugin.
     */
    public function registerComponents(): array
    {
        return [
            ViteAssets::class => 'viteAssets',
        ];
    }
}

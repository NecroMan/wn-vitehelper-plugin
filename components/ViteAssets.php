<?php namespace ASMilano\ViteHelper\Components;

use ASMilano\ViteHelper\Classes\ViteHelper;
use Cms\Classes\ComponentBase;

class ViteAssets extends ComponentBase
{
    public bool $isViteServed;
    public string $publicUrl;
    public string $entryScript;
    public array $js;
    public array $css;
    public string $render;

    /**
     * Gets the details for the component
     */
    public function componentDetails(): array
    {
        return [
            'name' => 'Vite Assets',
            'description' => 'Adds links to assets generated or served by Vite.',
        ];
    }

    public function defineProperties(): array
    {
        return [
            'render' => [
                'title' => 'Render type',
                'description' => 'Which files should be rendered in this component: js, css or all',
                'default' => 'all',
                'type' => 'dropdown',
                'options' => ['all' => 'JS and CSS', 'js' => 'JS', 'css' => 'CSS'],
            ],
        ];
    }

    public function init(): void
    {
        /** @var ViteHelper $helper */
        $helper = ViteHelper::instance();

        $this->isViteServed = $helper->getIsViteServed();
        $this->publicUrl = $helper->getPublicUrl();
        $this->entryScript = $helper->getEntryPath();
        $this->js = $helper->getJs();
        $this->css = $helper->getCss();
    }

    public function onRender(): void
    {
        $this->render = $this->property('render', 'all');
    }

    public function shouldRenderJs(): bool
    {
        return $this->render === 'all' || $this->render === 'js';
    }

    public function shouldRenderCss(): bool
    {
        return $this->render === 'all' || $this->render === 'css';
    }
}
